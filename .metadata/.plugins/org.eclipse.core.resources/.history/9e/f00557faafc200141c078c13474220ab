/*
 ===============================================================================
 Name        : adc-no-interrupt.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h> // implementa proteção de código à memória flash
#include "uart.h"

/* variáveis globais */
uint8_t adcChannel;

/* protótipos */
void adcInit(void);
void rightButtonsConfig(void);
void leftButtonsConfig(void);
void interruptButtonsConfig(void);
void sysTickConfig(void);
void delayUs(uint32_t);

/* isr de interrupção externa */
void PIOINT0_IRQHandler(void) {

}

/* isr de interrupção externa */
void PIOINT2_IRQHandler(void) {
	if (LPC_GPIO2->MIS & (1 << 5)) { // verifica se interrupção ocorreu no pino p2.4
		if (dataSending[0]) { // verifica se posição de dado referente ao status do led, está setada
			dataSending[0] = 0; // atribui 1 à posição do led
		} else {
			dataSending[0] = 1; // atribui 0 à posição do led
		}
		delayUs(177000); // delay para time debouncing
		LPC_GPIO2->IC |= (1 << 5); // limpa flag de interrupção do pino
	}
}

/* isr de interrupção externa */
void PIOINT3_IRQHandler(void) {

}

int main(void) {
	SystemInit();
	SystemCoreClockUpdate();
	uartInit();
	adcInit();
	rightButtonsConfig();
	leftButtonsConfig();
	interruptButtonsConfig();
	do { // loop infinito
		if (LPC_ADC->GDR & (1 << 31)) {
			adcChannel = ((LPC_ADC->GDR & (0b111 << 24)) >> 24); // pega canal em que ocorreu conversão
			if (!(LPC_ADC->STAT & (adcChannel << 8))) { // verifica se ocorreu overrun no canal
				printf("%s%u%s%u\n", "Valor convertido do canal ", adcChannel,
						": ", ((LPC_ADC->GDR & (0x3FF << 6)) >> 6));
			} else {
				printf("%s\n", "Ocorreu overun - valor descartado.");
			}
		}
	} while (1);
	return 1;
}

/* configura/inicializa adc */
void adcInit(void) {
	/* configurações gerais */
	LPC_SYSCON->PDRUNCFG &= ~(1 << 4); // limpando-se o bit, liga-se o periférico adc
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 13); // atribui-se clock ao adc
	LPC_IOCON->R_PIO1_2 = ((LPC_IOCON->R_PIO1_2
			& ~(0b11 << 0 | 0b11 << 3 | 1 << 7)) | (0b10 << 0)); // pino como ad3; desabilita pull-up e pull-down; seta admode
	/* configurações de operação */
	LPC_ADC->CR |= (1 << 3 | 1 << 8 | 1 << 16); // habilita canal 3; clkdiv = 1 (pclk_adc / 2); realiza conversão imediatamente
}

/* configura pinos dos botões da direita */
void rightButtonsConfig(void) {
	/* DIREITA - p2.8(branco), p2.5(rosa), p0.3 (azul), p3.4 (borboleta) */
	LPC_IOCON->PIO2_8 = ((LPC_IOCON->PIO2_8 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO2_5 = ((LPC_IOCON->PIO2_5 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO0_3 = ((LPC_IOCON->PIO0_3 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO3_4 = ((LPC_IOCON->PIO3_4 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_GPIO2->DIR &= ~(1 << 8 | 1 << 5); // pinos como entrada
	LPC_GPIO2->DATA |= (1 << 8 | 1 << 5); // estado lógico alto
	LPC_GPIO0->DIR &= ~(1 << 3); // pinos como entrada
	LPC_GPIO0->DATA |= (1 << 3); // estado lógico alto
	LPC_GPIO3->DIR &= ~(1 << 4); // pinos como entrada
	LPC_GPIO3->DATA |= (1 << 4); // estado lógico alto
}

/* configura pinos dos botões da esquerda */
void leftButtonsConfig(void) {

}

/* configura interrupções dos botões */
void interruptButtonsConfig(void) {
	/* configurações de interrupção */
	LPC_GPIO2->IE |= (1 << 8 | 1 << 5); // habilita interrupção nos pinos
	NVIC_EnableIRQ(EINT2_IRQn); // habilita interrupções no port2
	NVIC_SetPriority(EINT2_IRQn, 1); // atribui nível de prioridade 2 às interrupções no port2
	NVIC_ClearPendingIRQ(EINT2_IRQn); // retira qualquer pendência de interrupção no port2
}

/* configura systick */
void sysTickConfig(void) {

}
