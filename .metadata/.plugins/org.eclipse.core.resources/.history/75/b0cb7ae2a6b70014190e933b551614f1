/*
 ===============================================================================
 Name        : spi-test.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h> // implementa proteção de código à memória flash

/* protótipos */
void spiInit(void); // configura/inicializa spi
void spiPinsConfig(void); // configura pinos do spi
void sysTickConfig(void); // configura systick
void delayUs(unsigned int); // implementa delay em microssegundos
void uartInit(void); // configura/inicializa uart
void uartSend(unsigned int); // envia dado para buffer tx

int main(void) {
	SystemInit(); // inicializa sistema - c_clk = 48mhz
	SystemCoreClockUpdate(); // atualiza valor de clock para variável SystemCoreClock
	spiInit(); // configura spi
	spiPinsConfig(); // configura pinos spi
	sysTickConfig(); // configura systick
	uartInit(); // configura/inicializa uart
	do { // loop infinito
	} while (1);
	return 1;
}

/* configura/inicializa spi */
void spiInit(void) {

}

/* configura pinos do spi */
void spiPinsConfig(void) {
	/* p2.11 - sck */
	LPC_IOCON->SCK_LOC = ((LPC_IOCON->SCK_LOC & ~(0b11 << 0)) | (0b01 << 0)); // seleciona pino p2.11, para pino sck0
	LPC_IOCON->PIO2_11 = ((LPC_IOCON->PIO2_11 & ~(0b11 << 0)) | (0b01 << 0)); // pino como sck
	/* p0.9 - mosi/din */
	LPC_IOCON->PIO0_9 = ((LPC_IOCON->PIO0_9 & ~(0b11 << 0)) | (0b01 << 0)); // pino como mosi
	/* p0.2 - ssel/ce */
	LPC_IOCON->PIO0_2 = ((LPC_IOCON->PIO0_2 & ~(0b11 << 0)) | (0b01 << 0)); // pino como ssel0
	/* p3.0 - dc */
	LPC_IOCON->PIO3_0 &= ~(0b11 << 0); // pino como gpio
	/* p0.8 - reset */
	LPC_IOCON->PIO0_8 &= ~(0b11 << 0); // pino como gpio
}

/* configura systick */
void sysTickConfig(void) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // atribui valor de reinício para contador
	SysTick->CTRL |= (1 << 2); // fonte de decremento é c_clk
}

/* implementa delay em microssegundos */
void delayUs(unsigned int us) {
	unsigned int us; // contador de interrupção
	SysTick->CTRL |= (1 << 0); // habilita contador
	for (count = 0; count < us; count++) { // aguarda até que valor do contador se iguale ao valor recebido via parâmetro
		do { // aguarda até que flag de interrupção seja setada
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador
}

/* configura uart */
void uartInit(void) {
	/* configurações gerais */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 13); // habilita clock para periférico uart
	LPC_SYSCON->UARTCLKDIV = 2; // pclk_uart = c_clk / 2
	LPC_IOCON->PIO1_6 = ((LPC_IOCON->PIO1_6 & ~(0b11 << 0)) | (0b01 << 0)); // pino como rx
	LPC_IOCON->PIO1_7 = ((LPC_IOCON->PIO1_7 & ~(0b11 << 0)) | (0b01 << 0)); // pino como rx
	/* configurações de operação */
	LPC_UART->LCR |= (1 << 7); // seta dlab - permite acesso à divisores principais do brg
	LPC_UART->DLL = ((SystemCoreClock / 1000000) / (16 * 9600)); // lsb do divisor do brg
	LPC_UART->DLM = 0; // msb do divisor do brg
	LPC_UART->FDR |= (1 << 4); // divisor fracional do brg - cálculo: ((pclk_uart / (16 * 9600 * DLL)) - 1);
	LPC_UART->LCR &= ~(1 << 7); // limpa dlab - bloqueia acesso à divisores principais do brg
	/* configurações de pacote */
	LPC_UART->LCR |= (0b11 << 0); // dado trafegado será de 8 bits
	LPC_UART->FCR |= (1 << 0 | 1 << 1 | 1 << 2); // ativa e reinicia fifos
	LPC_UART->THR = 0; // limpa buffer tx
	LPC_UART->RBR; // limpa buffer rx
	LPC_UART->FCR &= ~(0b11 << 6); // dma habilitado - nível de disparo de 1 dado
	/* configurações de interrupção */
	LPC_UART->IER |= (1 << 0); // habilita interrupção por rbr
	NVIC_SetPriority(UART_IRQn, 2); // nível 2 de prioridade de interrupção
	NVIC_ClearPendingIRQ(UART_IRQn); // retira qualquer pendência de interrupção da uart
	NVIC_EnableIRQ(UART_IRQn); // habilita interrupções da uart
}
