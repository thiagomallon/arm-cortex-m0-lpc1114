/*
 ===============================================================================
 Name        : adc-no-interrupt.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h> // implementa proteção de código à memória flash
#include "uart.h"

/* variáveis globais */
uint8_t adcChannel;

/* protótipos */
void adcInit(void);
void rightButtonsConfig(void);
void leftButtonsConfig(void);
void interruptButtonsConfig(void);
void sysTickConfig(void);
void delayUs(uint32_t);

/* isr de interrupção externa */
void PIOINT2_IRQHandler(void) {
	if (LPC_GPIO2->MIS & (1 << 5)) { // verifica se interrupção ocorreu no pino p2.4
		printf("%s\n", "Botão rosa pressionado!");
	}
	if (LPC_GPIO2->MIS & (1 << 6)) { // verifica se interrupção ocorreu no pino p2.4
		printf("%s\n", "Botão azul pressionado!");
	}
	if (LPC_GPIO2->MIS & (1 << 8)) { // verifica se interrupção ocorreu no pino p2.4
		printf("%s\n", "Botão branco pressionado!");
	}
	delayUs(177000); // delay para time debouncing
	LPC_GPIO2->IC |= (1 << 5 | 1 << 6 | 1 << 8); // limpa flag de interrupção do pino
}

/* isr de interrupção externa */
void PIOINT3_IRQHandler(void) {
	if (LPC_GPIO3->MIS & (1 << 4)) { // verifica se interrupção ocorreu no pino p2.4
		printf("%s\n", "Botão borboleta dir. pressionado!");
	}
	delayUs(177000); // delay para time debouncing
	LPC_GPIO3->IC |= (1 << 4); // limpa flag de interrupção do pino
}

int main(void) {
	SystemInit();
	SystemCoreClockUpdate();
	sysTickConfig();
	uartInit();
	adcInit();
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 6); // habilita clock para pinos gpio
	rightButtonsConfig();
	leftButtonsConfig();
	interruptButtonsConfig();
	do { // loop infinito
		/*
		 if (LPC_ADC->GDR & (1 << 31)) {
		 adcChannel = ((LPC_ADC->GDR & (0b111 << 24)) >> 24); // pega canal em que ocorreu conversão
		 if (!(LPC_ADC->STAT & (adcChannel << 8))) { // verifica se ocorreu overrun no canal
		 printf("%s%u%s%u\n", "Valor convertido do canal ", adcChannel,
		 ": ", ((LPC_ADC->GDR & (0x3FF << 6)) >> 6));
		 } else {
		 printf("%s\n", "Ocorreu overun - valor descartado.");
		 }
		 }
		 */
	} while (1);
	return 1;
}

/* configura/inicializa adc */
void adcInit(void) {
	/* configurações gerais */
	LPC_SYSCON->PDRUNCFG &= ~(1 << 4); // limpando-se o bit, liga-se o periférico adc
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 13); // atribui-se clock ao adc
	LPC_IOCON->R_PIO1_2 = ((LPC_IOCON->R_PIO1_2
			& ~(0b11 << 0 | 0b11 << 3 | 1 << 7)) | (0b10 << 0)); // pino como ad3; desabilita pull-up e pull-down; seta admode
	/* configurações de operação */
	LPC_ADC->CR |= (1 << 3 | 1 << 8 | 1 << 16); // habilita canal 3; clkdiv = 1 (pclk_adc / 2); realiza conversão imediatamente
}

/* configura pinos dos botões da direita */
void rightButtonsConfig(void) {
	/* DIREITA - p2.8(branco), p2.5(rosa), p2.6 (azul), p3.4 (borboleta) */
	LPC_IOCON->PIO2_8 = ((LPC_IOCON->PIO2_8 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO2_5 = ((LPC_IOCON->PIO2_5 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO2_6 = ((LPC_IOCON->PIO2_6 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO3_4 = ((LPC_IOCON->PIO3_4 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_GPIO2->DIR &= ~(1 << 8 | 1 << 6 | 1 << 5); // pinos como entrada
	LPC_GPIO2->DATA |= (1 << 8 | 1 << 6 | 1 << 5); // estado lógico alto
	LPC_GPIO3->DIR &= ~(1 << 4); // pinos como entrada
	LPC_GPIO3->DATA |= (1 << 4); // estado lógico alto
}

/* configura pinos dos botões da esquerda */
void leftButtonsConfig(void) {

}

/* configura interrupções dos botões */
void interruptButtonsConfig(void) {
	/* configurações de interrupção p2 */
	LPC_GPIO2->IE |= (1 << 8 | 1 << 6 | 1 << 5); // habilita interrupção nos pinos
	LPC_GPIO2->IS &= ~(1 << 8 | 1 << 6 | 1 << 5); // interrupção por mudança de borda
	LPC_GPIO2->IBE &= ~(1 << 8 | 1 << 6 | 1 << 5); // interrupção será em borda específica
	LPC_GPIO2->IEV &= ~(1 << 8 | 1 << 6 | 1 << 5); // interrupção será por borda de descida
	NVIC_EnableIRQ(EINT2_IRQn); // habilita interrupções no port2
	NVIC_SetPriority(EINT2_IRQn, 1); // atribui nível de prioridade às interrupções no port2
	NVIC_ClearPendingIRQ(EINT2_IRQn); // retira qualquer pendência de interrupção no port2
	/* configurações de interrupção p1 */
	LPC_GPIO3->IE |= (1 << 4); // habilita interrupção nos pinos
	LPC_GPIO3->IS &= ~(1 << 4); // interrupção por mudança de borda
	LPC_GPIO3->IBE &= ~(1 << 4); // interrupção será em borda específica
	LPC_GPIO3->IEV &= ~(1 << 4); // interrupção será por borda de descida
	NVIC_EnableIRQ(EINT3_IRQn); // habilita interrupções no port3
	NVIC_SetPriority(EINT3_IRQn, 1); // atribui nível de prioridade às interrupções no port3
	NVIC_ClearPendingIRQ(EINT3_IRQn); // retira qualquer pendência de interrupção no port3
}

/* configura systick */
void sysTickConfig(void) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // valor de reinício para o contador
	SysTick->CTRL |= (1 << 2); // fonte de decremento é c_clk
}

/* implementa delay em microssegundos */
void delayUs(uint32_t us) {
	static uint32_t count; // contador de 'zeramentos'
	SysTick->CTRL |= (1 << 0); // inicializa contador
	for (count = 0; count < us; count++) { // aguarda até que valor do contador se iguale à valor recebido via parâmetro
		do { // aguarda até que flag de zeramento esteja setada
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador
}
