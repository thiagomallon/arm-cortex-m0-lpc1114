/*
 ===============================================================================
 Name        : spi-test.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h> // implementa proteção de código à memória flash
#include "uart.h" // importa arquivo de inicialização da uart
#include "glcd.h" // importa arquivo de configuração/iteração com glcd

/* protótipos */
void spiInit(void); // configura/inicializa spi
void spiPinsConfig(void); // configura pinos do spi
void sysTickConfig(void); // configura systick
void delayUs(unsigned int); // implementa delay em microssegundos

int main(void) {
	SystemInit(); // inicializa sistema - c_clk = 48mhz
	SystemCoreClockUpdate(); // atualiza valor de clock para variável SystemCoreClock
	spiInit(); // configura spi
	spiPinsConfig(); // configura pinos spi
	sysTickConfig(); // configura systick
	uartInit(); // configura/inicializa uart
	glcdInit(); // configura/inicaliza glcd
	do { // loop infinito
		delayUs(500000);
		printf("%s\n", "Teste");
	} while (1);
	return 1;
}

/* configura/inicializa spi */
void spiInit(void) {
	/* configurações gerais */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 11); // habilita clock para ssp0
	LPC_SYSCON->SSP0CLKDIV = 4; // pclk_ssp0 = c_clk / 4
	/* configurações de operação */
	LPC_SYSCON->PRESETCTRL |= (1 << 0); // escreve 1 para os sinais de reset
	LPC_SSP0->CR0 |= (0b111 << 0 | 0b00 << 4 | 0 << 6 | 0 << 7); // dado de 8 bits; formato spi de frame; cpol = 0; cpha = 0
	LPC_SSP0->CR1 |= (1 << 1 | 0 << 2); // habilita spi ;modo master
}

/* configura pinos do spi */
void spiPinsConfig(void) {
	/* p2.11 - sck */
	LPC_IOCON->SCK_LOC = ((LPC_IOCON->SCK_LOC & ~(0b11 << 0)) | (0b01 << 0)); // seleciona pino p2.11, para pino sck0
	LPC_IOCON->PIO2_11 = ((LPC_IOCON->PIO2_11 & ~(0b11 << 0)) | (0b01 << 0)); // pino como sck
	/* p0.9 - mosi/din */
	LPC_IOCON->PIO0_9 = ((LPC_IOCON->PIO0_9 & ~(0b11 << 0)) | (0b01 << 0)); // pino como mosi
	/* p0.2 - CE (quando low permite recepção de dados, quando high apenas exibe) */
	LPC_IOCON->PIO0_2 &= ~(0b11 << 0); // pino como gpio
	LPC_GPIO0->DIR |= (1 << 2); // pino como saída
	LPC_GPIO0->DATA |= (1 << 2); // estado lógico alto
	/* p3.0 - DC (quando low espera comando, quando high espera dado) */
	LPC_IOCON->PIO3_0 &= ~(0b11 << 0); // pino como gpio
	LPC_GPIO3->DIR |= (1 << 0); // pino como saída
	LPC_GPIO3->DATA &= ~(1 << 0); // estado lógico baixo
	/* p0.8 - RST (quando low, reseta o glcd) */
	LPC_IOCON->PIO0_8 &= ~(0b11 << 0); // pino como gpio
	LPC_GPIO0->DIR |= (1 << 8); // pino como saída
	LPC_GPIO0->DATA |= (1 << 8); // estado lógico alto
}

/* configura systick */
void sysTickConfig(void) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // atribui valor de reinício para contador
	SysTick->CTRL |= (1 << 2); // fonte de decremento é c_clk
}

/* implementa delay em microssegundos */
void delayUs(unsigned int us) {
	unsigned int count; // contador de interrupção
	SysTick->CTRL |= (1 << 0); // habilita contador
	for (count = 0; count < us; count++) { // aguarda até que valor do contador se iguale ao valor recebido via parâmetro
		do { // aguarda até que flag de interrupção seja setada
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador
}

