/*
 ===============================================================================
 Name        : adc-no-interrupt.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h> // implementa proteção de código à memória flash
#include "uart.h"

/* configurações gerais */
#define CHANNEL 107 /* valor entre 1 e 126 - deve ser o mesmo para TX e RX*/
#define BUFFER_SIZE 32 /* valor entre 1 e 32 - tamanho do buffer */
#define ADDRESS 75 /* valor entre 1 e 255 */

/* variáveis globais */
uint8_t adChannel;
uint8_t selectedChannel = 0;
uint8_t adChannels[3] = { 0, 0, 0 };
uint8_t dataSending[BUFFER_SIZE]; // matriz armazena dados recebidos

/* protótipos */
void adcInit(void);
void buttonsConfig(void);
void interruptButtonsConfig(void);
void sysTickConfig(void);
void delayUs(uint32_t);

/* isr de interrupção externa */
void PIOINT2_IRQHandler(void) {
	if (LPC_GPIO2->MIS & (1 << 7)) { // verifica se interrupção ocorreu no pino p2.4
		printf("%s\n%s%u\n%s%u\n%s%u\n\n", "Botão L1 pressionado!",
				"Valor direção: ", adChannels[0], "Valor aceleração: ",
				adChannels[2], "Valor frenagem: ", adChannels[1]);
	}
	if (LPC_GPIO2->MIS & (1 << 9)) { // verifica se interrupção ocorreu no pino p2.4
		printf("%s\n%s%u\n%s%u\n%s%u\n\n", "Botão R1 pressionado!",
				"Valor direção: ", adChannels[0], "Valor aceleração: ",
				adChannels[2], "Valor frenagem: ", adChannels[1]);
	}
	if (LPC_GPIO2->MIS & (1 << 8)) { // verifica se interrupção ocorreu no pino p2.4
		printf("%s\n%s%u\n%s%u\n%s%u\n\n", "Botão roxo pressionado!",
				"Valor direção: ", adChannels[0], "Valor aceleração: ",
				adChannels[2], "Valor frenagem: ", adChannels[1]);
	}
	if (LPC_GPIO2->MIS & (1 << 10)) { // verifica se interrupção ocorreu no pino p2.4
		printf("%s\n%s%u\n%s%u\n%s%u\n\n", "Borboleta esq. pressionada!",
				"Valor direção: ", adChannels[0], "Valor aceleração: ",
				adChannels[2], "Valor frenagem: ", adChannels[1]);
	}
	delayUs(177000); // delay para time debouncing
	LPC_GPIO2->IC |= (1 << 7 | 1 << 8 | 1 << 9 | 1 << 10); // limpa flag de interrupção do pino
}

/* isr de interrupção externa */
void PIOINT3_IRQHandler(void) {
	if (LPC_GPIO3->MIS & (1 << 0)) { // verifica se interrupção ocorreu no pino p2.4
		printf("%s\n%s%u\n%s%u\n%s%u\n\n", "Borboleta dir. pressionada!",
				"Valor direção: ", adChannels[0], "Valor aceleração: ",
				adChannels[2], "Valor frenagem: ", adChannels[1]);
	}
	if (LPC_GPIO3->MIS & (1 << 4)) { // verifica se interrupção ocorreu no pino p2.4
		printf("%s\n%s%u\n%s%u\n%s%u\n\n", "Botão azul pressionado!",
				"Valor direção: ", adChannels[0], "Valor aceleração: ",
				adChannels[2], "Valor frenagem: ", adChannels[1]);
	}
	delayUs(177000); // delay para time debouncing
	LPC_GPIO3->IC |= (1 << 0 | 1 << 4); // limpa flag de interrupção do pino
}

int main(void) {
	SystemInit();
	SystemCoreClockUpdate();
	sysTickConfig();
	uartInit();
	adcInit();
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 6); // habilita clock para pinos gpio
	buttonsConfig();
	interruptButtonsConfig();
	do { // loop infinito
		if (LPC_ADC->GDR & (1 << 31)) {
			adChannel = ((LPC_ADC->GDR & (0b111 << 24)) >> 24); // pega canal em que ocorreu conversão
			if (!(LPC_ADC->STAT & (adChannel << 8))) { // verifica se ocorreu overrun no canal
				adChannels[adChannel] = ((LPC_ADC->GDR & (0x3FF << 6)) >> 6)
						/ 5;
			}
		}
	} while (1);
	return 1;
}

/* configura/inicializa adc */
void adcInit(void) {
	/* configurações gerais */
	LPC_SYSCON->PDRUNCFG &= ~(1 << 4); // limpando-se o bit, liga-se o periférico adc
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 13); // atribui-se clock ao adc
	/* canal 0 */
	LPC_IOCON->R_PIO0_11 = ((LPC_IOCON->R_PIO0_11
			& ~(0b11 << 0 | 0b11 << 3 | 1 << 7)) | (0b10 << 0)); // pino como ad3; desabilita pull-up e pull-down; seta admode
	/* canal 1 */
	LPC_IOCON->R_PIO1_0 = ((LPC_IOCON->R_PIO1_0
			& ~(0b11 << 0 | 0b11 << 3 | 1 << 7)) | (0b10 << 0)); // pino como ad3; desabilita pull-up e pull-down; seta admode
	/* canal 2 */
	LPC_IOCON->R_PIO1_1 = ((LPC_IOCON->R_PIO1_1
			& ~(0b11 << 0 | 0b11 << 3 | 1 << 7)) | (0b10 << 0)); // pino como ad3; desabilita pull-up e pull-down; seta admode
	/* configurações de operação */
	LPC_ADC->CR |= (1 << 0 | 1 << 8 | 1 << 16); // habilita canal; clkdiv = 1 (pclk_adc / 2); modo burst
}

/* configura pinos dos botões da direita */
void buttonsConfig(void) {
	/* DIREITA - p2.9(R1), p2.7(L1), p3.0(Borb. right), p3.4(Azul), p2.10(Borb. left), p2.8(Roxo) */
	LPC_IOCON->PIO2_9 = ((LPC_IOCON->PIO2_9 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO2_7 = ((LPC_IOCON->PIO2_7 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO3_0 = ((LPC_IOCON->PIO3_0 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO3_4 = ((LPC_IOCON->PIO3_4 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO2_10 = ((LPC_IOCON->PIO2_10 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_IOCON->PIO2_8 = ((LPC_IOCON->PIO2_8 & ~(0b11 << 3 | 0b11 << 0))
			| (0b10 << 3)); // pull-up ativo; pino como gpio
	LPC_GPIO2->DIR &= ~(1 << 10 | 1 << 9 | 1 << 8 | 1 << 7); // pinos como entrada
	LPC_GPIO2->DATA |= (1 << 10 | 1 << 9 | 1 << 8 | 1 << 7); // estado lógico alto
	LPC_GPIO3->DIR &= ~(1 << 0 | 1 << 4); // pinos como entrada
	LPC_GPIO3->DATA |= (1 << 0 | 1 << 4); // estado lógico alto
}

/* configura interrupções dos botões */
void interruptButtonsConfig(void) {
	/* configurações de interrupção p2 */
	LPC_GPIO2->IE |= (1 << 10 | 1 << 9 | 1 << 8 | 1 << 7); // habilita interrupção nos pinos
	LPC_GPIO2->IS &= ~(1 << 10 | 1 << 9 | 1 << 8 | 1 << 7); // interrupção por mudança de borda
	LPC_GPIO2->IBE &= ~(1 << 10 | 1 << 9 | 1 << 8 | 1 << 7); // interrupção será em borda específica
	LPC_GPIO2->IEV &= ~(1 << 10 | 1 << 9 | 1 << 8 | 1 << 7); // interrupção será por borda de descida
	NVIC_EnableIRQ(EINT2_IRQn); // habilita interrupções no port2
	NVIC_SetPriority(EINT2_IRQn, 1); // atribui nível de prioridade às interrupções no port2
	NVIC_ClearPendingIRQ(EINT2_IRQn); // retira qualquer pendência de interrupção no port2
	/* configurações de interrupção p1 */
	LPC_GPIO3->IE |= (1 << 0 | 1 << 4); // habilita interrupção nos pinos
	LPC_GPIO3->IS &= ~(1 << 0 | 1 << 4); // interrupção por mudança de borda
	LPC_GPIO3->IBE &= ~(1 << 0 | 1 << 4); // interrupção será em borda específica
	LPC_GPIO3->IEV &= ~(1 << 0 | 1 << 4); // interrupção será por borda de descida
	NVIC_EnableIRQ(EINT3_IRQn); // habilita interrupções no port3
	NVIC_SetPriority(EINT3_IRQn, 1); // atribui nível de prioridade às interrupções no port3
	NVIC_ClearPendingIRQ(EINT3_IRQn); // retira qualquer pendência de interrupção no port3
}

/* configura systick */
void sysTickConfig(void) {
	SysTick->LOAD = (SystemCoreClock / 1000000) - 1; // valor de reinício para o contador
	SysTick->CTRL |= (1 << 2); // fonte de decremento é c_clk
}

/* implementa delay em microssegundos */
void delayUs(uint32_t us) {
	static uint32_t count; // contador de 'zeramentos'
	SysTick->CTRL |= (1 << 0); // inicializa contador
	for (count = 0; count < us; count++) { // aguarda até que valor do contador se iguale à valor recebido via parâmetro
		do { // aguarda até que flag de zeramento esteja setada
		} while (!(SysTick->CTRL & (1 << 16)));
	}
	SysTick->CTRL &= ~(1 << 0); // para contador
}
